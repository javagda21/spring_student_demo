package com.j21.spring_student_demo.service;

import com.j21.spring_student_demo.model.Student;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class StudentService {
    // bean - fasolka / ziarno - w javie beany to encje które są przechowywane
//            przez moduł DI (Dependency Injection) i mogą być z niego wybierane
//            w celu uzupełnienia zależności
//      są singletonami (nazwanymi - posiadają nazwy)

    private List<Student> students = new ArrayList<>();

    public StudentService() {
        students.add(new Student(null, "a", "b", new ArrayList<>(Arrays.asList(2.0, 3.0, 4.0, 5.0))));
        students.add(new Student(null, "c", "d", new ArrayList<>(Arrays.asList(1.8, 3.1, 4.1, 5.0))));
    }

    // create
    public void add(Student s) {
        // dodaj studenta do listy.
        students.add(s);
    }

    public List<Student> read() {
        return students;
    }

    public void update(int position, Student update) {

    }

    // delete
    public void remove(int position) {
        students.remove(position);
    }

    public void addGradeToStudentAtPosition(double grade, int position) {
        students.get(position).getGrades().add(grade);
    }
}
