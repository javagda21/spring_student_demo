package com.j21.spring_student_demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private Long id;

    private String name;
    private String surname;

    private List<Double> grades;
}
