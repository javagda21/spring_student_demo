package com.j21.spring_student_demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MultiplyController {

    // 2 mappingi: jeden GET, drugi POST
    // oba mappingi na adres /multiply
    // wewnątrz strony GET zrobimy formularz z dwiema liczbami
    // wewnątrz POST zrobimy sout tych parametrów.

    // pamiętaj by stworzyć formularz (form) w html!
    @RequestMapping(path = "/multiply", method = RequestMethod.GET)
    public String zaladujFormularzTabliczkiMnozenia() {

        return "multiplyForm";
    }

    @RequestMapping(path = "/multiply", method = RequestMethod.POST)
    public String zaladujFormularz(@RequestParam(name = "zmienna_x") int zmiennaX,
                                   @RequestParam(name = "zmienna_y") int zmiennaY,
                                   Model model) {

        System.out.println("X: " + zmiennaX);
        System.out.println("Y: " + zmiennaY);

        model.addAttribute("attr_x", zmiennaX); // attribute!
        model.addAttribute("attr_y", zmiennaY); // attribute!

        return "multiplyForm";
    }

}
