package com.j21.spring_student_demo.controller;

import com.j21.spring_student_demo.model.Student;
import com.j21.spring_student_demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping("/student/list")
    public String getStudentList(Model model) {
        // przekazujemy (wysyłamy do widoku/htmla) listę studentów
        model.addAttribute("studentList", studentService.read());

        return "studentListPage";
    }

    @GetMapping("/student/add")
    public String getStudentForm() {
        return "studentFormPage";
    }

    @PostMapping("/student/add")
    public String submitStudentForm(@RequestParam(name = "zmiennaImie") String imie,
                                    @RequestParam(name = "zmiennaNazwisko") String nazwisko) {
        Student s = new Student(null, imie, nazwisko, new ArrayList<>());

        studentService.add(s);

        return "redirect:/student/list";
    }

    @GetMapping("/student/remove")
    public String removeStudentAtPosition(
            @RequestParam(name = "studentId") int position) {
        studentService.remove(position);

        return "redirect:/student/list";
    }

    @GetMapping("/student/addGrade")
    public String addGrade(@RequestParam(name = "studentId") int position,
                           Model model) {
        model.addAttribute("studentPos", position);

        return "studentGradeForm";
    }

    @PostMapping("/student/addGrade")
    public String addGrade(@RequestParam(name = "grade") double grade,
                           @RequestParam(name = "positionOfStudent") int position) {

        // todo: metoda w serwisie która służy do dodania oceny 'grade' studentowi który jest w
//          tamtejszej liście na pozycji 'position'
        studentService.addGradeToStudentAtPosition(grade, position);

        return "redirect:/student/list";
    }

}
