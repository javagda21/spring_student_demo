package com.j21.spring_student_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringStudentDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringStudentDemoApplication.class, args);
    }

}
